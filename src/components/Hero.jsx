import React from 'react'

export default function Hero() {
    
    return (
        <div className='max-w-[1640px] mx-auto p-4 h-full'>
            <div className='max-h-[500px] relative '>
                {/* Overlay */}
                <div className='absolute w-full h-full text-gray-200 max-h-[500px] bg-black/20 flex flex-col justify-center'>
                    <h1 className='px-4 text-4xl sm:text-5xl md:text-6xl lg:text-7xl font-bold'>Super <span className='text-orange-500'>Eats</span></h1>
                    <h1 className='px-4 text-4xl sm:text-5xl md:text-6xl lg:text-7xl font-bold'><span className='text-orange-500'>Foods </span> Delivered</h1>
                </div>
                {/* Getting images from pexels or unsplash */}
                {/* object cover maintains the ratio of the image */}
                <img className="object-cover h-[500px] w-full" src="https://images.unsplash.com/photo-1529687891765-e1c10601a14f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2076&q=80" alt="Image of baker cutting into pastry" />
            </div>
        </div>
    )
}
