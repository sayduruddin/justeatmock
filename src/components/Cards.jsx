import React from 'react'

export default function Cards() {
    // Initial creation of Cards component
    return (
        <div className='max-w-[1640px] mx-auto p-4 py-12 grid md:grid-cols-3 gap-4'>
            {/* Card */}
            <div className='rounded-xl relative'>
                {/* Overlay div */}
                <div className='rounded-xl absolute w-full h-full bg-black/30 text-white'>
                    <p className='font-semibold text-2xl px-2 pt-4 text-orange-400'>Offer 1</p>
                    <p className='px-2 italic'>Through to the end of the month</p>
                    <button className='button border-white bg-white/50 text-black mx-2 absolute bottom-4 hover:bg-orange-50'>Order Now</button>
                </div>
                <img className='rounded-xl object-cover max-h-[160px] md:max-h-[200px] w-full' src="https://images.unsplash.com/photo-1588315029754-2dd089d39a1a?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2071&q=80" alt="Slicing of pizza" />
            </div>

            {/* Card 2 */}
            {/* Card */}
            <div className='rounded-xl relative'>
                {/* Overlay div */}
                <div className='rounded-xl absolute w-full h-full bg-black/30 text-white'>
                    <p className='font-semibold text-2xl px-2 pt-4 text-orange-400'>Offer 2</p>
                    <p className='px-2 italic'>Through to the end of the week</p>
                    <button className='button border-white bg-white/50 text-black mx-2 absolute bottom-4 hover:bg-orange-50'>Order Now</button>
                </div>
                <img className='rounded-xl object-cover max-h-[160px] md:max-h-[200px] w-full' src="https://plus.unsplash.com/premium_photo-1671403963864-6d46f3b62352?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1042&q=80" alt="Salad" />
            </div>

            {/* Card 3 */}
            {/* Card */}
            <div className='rounded-xl relative'>
                {/* Overlay div */}
                <div className='rounded-xl absolute w-full h-full bg-black/30 text-white'>
                    <p className='font-semibold text-2xl px-2 pt-4 text-orange-400'>Offer 3</p>
                    <p className='px-2 italic'>Special Members offer</p>
                    <button className='button border-white bg-white/50 text-black mx-2 absolute bottom-4 hover:bg-orange-50'>Order Now</button>
                </div>
                <img className='rounded-xl object-cover max-h-[160px] md:max-h-[200px] w-full' src="https://images.unsplash.com/photo-1594212699903-ec8a3eca50f5?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2071&q=80" alt="Burger and fries" />
            </div>

        </div>
    )
}