import React from 'react'
import {categories} from '../data/data.js';


export default function Category() {
    
    return (
        <div className='max-w-[1640px] px-4 py-4 m-auto'>
            <h1 className='text-orange-500 font-bold text-3xl text-center'>Top Rated Menu Items</h1>
            {/* Categories */}
            <div className='grid grid-cols-3 md:grid-cols-4 gap-6 py-4'>
                {/* We would usually use curly braces here, but since we are returning some div elements, we have to use parentheses for the arrow expression */}
                {categories.map( (item) => (
                    <div key={item.id} className='bg-gray-100 rounded-lg p-4 flex justify-between items-center'>
                        <h2 className='font-semibold '>{item.name}</h2>
                        <img src={item.image} alt={item.name} className='w-[5rem]'/>
                    </div>

                ) )}
            </div>

        </div>
    )
}
