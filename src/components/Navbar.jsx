import React, { useState } from "react";
import { AiOutlineMenu, AiOutlineSearch, AiOutlineClose } from 'react-icons/ai';
import { BsFillCartFill } from 'react-icons/bs';
import { TbTruckDelivery, TbWallet, TbTags, TbDownload, TbFriends } from 'react-icons/tb'
import { MdFavorite, MdHelp } from 'react-icons/md';

function Navbar() {

    const [nav, setNav] = useState(false);

    return (

        <div className="max-w-[1640px] mx-auto flex justify-between items-center p-4">

            {/* Left side */}
            <div className="flex items-center">
                <div className="cursor-pointer">
                    <AiOutlineMenu onClick={() => { setNav(!nav) }} 
                    size={30} 
                    style={{ color: 'orange'}}/>
                </div>
                <h1 className="text-orange-500 ml-1 text-2xl sm:text-3xl lg:text-4xl">Super <span className="font-bold">Eats</span>
                </h1>
                <div className="hidden lg:flex items-center bg-gray-100 rounded-full p-1 text-[14px]">
                    <p className="bg-orange-500 text-white rounded-full p-2">Delivery</p>
                    <p className="p-2">Collection</p>
                </div>
            </div>

            {/* Search input bar */}
            <div className="bg-gray-100 rounded-full flex items-center px-2 w-[200px] sm:w-[400px] lg:w-[500px]">
                <AiOutlineSearch size={30} style={{ color: 'orange' }}/>
                <input className="bg-transparent p-2 focus: outline-none w-full text-orange-500" type="text" placeholder="Search items" />
            </div>

            {/* Cart button */}
            <button className="bg-white text-orange-500 hidden md:flex items-center border-orange-500 py-2 rounded-full">
                <BsFillCartFill size={20} className="mr-2"/>Basket
            </button>


            {/* Overlay menu, this part will give the effect that something has overlayed on top of the window on button click, want this part to be displayed when burger icon is clicked for side panel */}
            {nav ? <div className="bg-black/80 fixed w-full h-screen z-10 top-0 left-0"></div> : ''}
            
            {/* Side drawer stuff */}
            <div className={nav ? "fixed top-0 left-0 w-[250px] h-screen bg-white z-10 duration-300" : "fixed top-0 left-[-250px] w-[250px] h-screen bg-white z-10 duration-300 hidden"}>
                <AiOutlineClose onClick={() => {setNav(!nav)}} 
                size={35} 
                className="absolute right-4 top-4 cursor-pointer"
                style={{ color: 'orange'}}/>
                <h2 className="text-orange-500 text-2xl p-4">Super <span className="font-bold">Eats</span></h2>
                <nav>
                    <ul className="flex flex-col p-2 text-gray-800 italic justify-center items-center">
                        <li className="sidepanelItems"><TbTruckDelivery size={30} className="mr-2"/>Orders</li>
                        <li className="sidepanelItems"><MdFavorite size={30} className="mr-2"/>Favourites</li>
                        <li className="sidepanelItems"><TbWallet size={30} className="mr-2"/>Wallet</li>
                        <li className="sidepanelItems"><MdHelp size={30} className="mr-2"/>Help</li>
                        <li className="sidepanelItems"><TbTags size={30} className="mr-2"/>Promotions</li>
                        <li className="sidepanelItems"><TbDownload size={30} className="mr-2"/>Deal</li>
                        <li className="sidepanelItems"><TbFriends size={30} className="mr-2"/>Invite Friends</li>
                    </ul>
                </nav>
            </div>

        </div>
    )

}

export default Navbar