import React, { useState } from 'react'
import {data} from '../data/data.js' 


export default function Food() {
    // By default, foods is set to data
    const [foods, setFoods] = useState(data);

    // Filtering by type or price
    function filterType(category) {
        setFoods(
            data.filter( (item) => {
                return item.category === category;
            } )
        );
    } 

    function filterPrice(price) {
        setFoods(
            data.filter( (item) => {
                return item.price === price;
            } )
        );
    }

    return (
        <div className='max-w-[1640px] m-auto px-6 py-6 p-2'>
            <h1 className='text-orange-500 font-semibold text-3xl text-center'>Popular Items</h1>

            {/* filter row */}
            <div className='flex flex-col lg:flex-row justify-between'>

                {/* filter type */}
                <div>
                    <p className='font-semibold text-gray-600'>Filter by type</p>
                    <div className='flex flex-wrap justify-between max-w-[500px]'>
                        <button onClick={() => { setFoods(data) }} className='m-1 hover:bg-gray-300 hover:text-orange-600  border-orange-500 bg-white text-orange-500 '>All</button>
                        <button onClick={() => { filterType('burger') }} className='m-1 hover:bg-gray-300 hover:text-orange-600 border-orange-500 bg-white text-orange-500 '>Burger</button>
                        <button onClick={() => { filterType('pizza') }} className='m-1 hover:bg-gray-300 hover:text-orange-600 border-orange-500 bg-white text-orange-500 '>Pizza</button>
                        <button onClick={() => { filterType('salad') }} className='m-1 hover:bg-gray-300 hover:text-orange-600 border-orange-500 bg-white text-orange-500 '>Salad</button>
                        <button onClick={() => { filterType('chicken') }} className='m-1 hover:bg-gray-300 hover:text-orange-600 border-orange-500 bg-white text-orange-500 '>Chicken</button>
                    </div>
                </div>

                {/* filter price */}
                <div>
                    <p className='font-semibold text-gray-600'>Filter Price</p>
                    <div className='flex flex-wrap justify-between max-w-[300px]'>
                        <button onClick={() => { filterPrice('£') } } className='m-1 hover:bg-gray-300 hover:text-orange-600 border-orange-500 bg-white text-orange-500 '>£</button>
                        <button onClick={() => { filterPrice('££') } } className='m-1 hover:bg-gray-300 hover:text-orange-600 border-orange-500 bg-white text-orange-500 '>££</button>
                        <button onClick={() => { filterPrice('£££') } } className='m-1 hover:bg-gray-300 hover:text-orange-600 border-orange-500 bg-white text-orange-500 '>£££</button>
                        <button onClick={() => { filterPrice('££££') } } className='m-1 hover:bg-gray-300 hover:text-orange-600 border-orange-500 bg-white text-orange-500 '>££££</button>
                    </div>
                </div>
            </div>

            {/* Display the food below the filtering buttons container */}
            <div className='grid grid-cols-2 lg:grid-cols-3 gap-5 pt-2'>

                { foods.map( (item, index) => (
                    <div
                        className='border rounded-lg shadow-lg hover:scale-105 duration-300' 
                        key={index}>
                        <img src={item.image} alt={item.name} className='w-full h-[200px] object-cover rounded-t-lg '/>
                        <div className='flex justify-between items-center'>
                            <p className='text-lg p-2 text-gray-800'>{item.name}</p>
                            <p><span className='mr-2 bg-orange-500 text-white rounded-full p-1 font-thin'>{item.price}</span>
                            </p>
                        </div>
                        
                    </div>
                ) ) }

            </div>

        </div>
    )
}
