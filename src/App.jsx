import React from "react"
import Navbar from "./components/Navbar"
import Hero from "./components/Hero"
import Cards from "./components/Cards"
import Food from "./components/Food"
import Category from "./components/Category"

function App() {

  return (
    <>
      <Navbar />
      <Hero />
      <Cards />
      <Food />
      <Category />
    </>
  )
}

export default App
